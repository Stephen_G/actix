use actix_web::{web, App, HttpRequest, HttpServer, Responder};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Debug, Clone, Deserialize)]
struct PostValues {
    #[serde(flatten)]
    values: HashMap<String, String>,
}

async fn get_handler(_request: HttpRequest) -> impl Responder {
    web::HttpResponse::Ok().body(
        r#"<!DOCTYPE html>
<hmtl>
  <head>
    <title>My first HTML</title>
    </head>
  <body>
<form action="" method="POST">
    <div>
        <label for="username">Username:</label>
        <input type="text" name="username">
    </div>
    <div>
        <label for="email">E-mail:</label>
        <input type="email" name="email">
    </div>
    <div>
        <label for="password">Password:</label>
        <input type="password" name="password">
    </div>

    <input type="submit" value="Sign Up">
</form>
  </body>
 </html>
"#,
    )
}

async fn post_handler(data: web::Form<PostValues>) -> impl Responder {
    println!("{:?}", data.values);
    format!("Posted values are: {:#?}", data.values)
}

#[actix_rt::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(move || {
        App::new()
            .route("/", web::get().to(get_handler))
            .route("/", web::post().to(post_handler))
    })
    .bind("127.0.0.1:8000")?
    .run()
    .await
}
